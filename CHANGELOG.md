# Changelog for issue-sync

<!--
Copyright 2024, Collabora, Ltd.
SPDX-License-Identifier: CC-BY-4.0
-->

An abbreviated summary of user-facing changes per release tag.

## 3.1.0 (2024-08-08)

- Allow providing an array of labels to apply to created GitLab issues, instead of just one label.

## 3.0.1 (2024-08-08)

- Housekeeping and compatibility updates.
- Do not sync "dependabot" PRs.
- Note: This version had been used in production essentially intact for several years without a tag.

## 3.0.0 (2021-04-16)

- Restore automatic usage of `.env` files.
- Data file schema changes.

## Previously

Ancient history by now.
