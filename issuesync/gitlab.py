#!/usr/bin/env python3 -i
# Copyright (c) 2019 Collabora, Ltd.
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author: Rylie Pavlik <rylie.pavlik@collabora.com>
#
# Requires Python 3.7 or newer

import asyncio
import os
import urllib.parse
from typing import Dict, Optional

import aiohttp
import gidgetlab
from gidgetlab.aiohttp import GitLabAPI

from .log import config_logger, make_logger


class GitLabAccess:
    def __init__(self, url: str, username: str, token: str):
        self.__logger = make_logger(__name__)
        self.url = url
        self.token = token
        if not self.token:
            raise RuntimeError("Must specify GL_ACCESS_TOKEN!")
        self.username = username
        if not self.username:
            raise RuntimeError("Must specify GL_USERNAME!")
        self.__logger.info('GitLab access: %s as %s', self.url, self.username)

    @classmethod
    def from_environment(cls):
        url = os.getenv('GL_URL', 'https://gitlab.com')
        token = os.getenv("GL_ACCESS_TOKEN")
        if not token:
            raise RuntimeError("Must specify GL_ACCESS_TOKEN!")
        username = os.getenv('GL_USERNAME')
        if not username:
            raise RuntimeError("Must specify GL_USERNAME!")
        return GitLabAccess(token=token, username=username, url=url)

    def make_api(self, session):
        return GitLabAPI(session, self.username, url=self.url,
                         access_token=self.token)

    async def _wrapper(self, func):
        async with aiohttp.ClientSession() as session:
            gl = self.make_api(session=session)
            return await func(gl)

    def call_with_gitlab(self, func):
        """Call the given function in an async environment.

        Passes a GitLabAPI instance as an argument.
        """
        loop = asyncio.get_event_loop()
        return loop.run_until_complete(self._wrapper(func))


def _gitlab_urlencode(s: str):
    """URL encode a string, including the slashes."""
    # safe='' so that it escapes slashes too.
    return urllib.parse.quote(s, safe='')


def make_gl_client():
    from dotenv import load_dotenv
    load_dotenv()
    # private_token = os.getenv('GL_PRIVATE_TOKEN', None)
    # oauth_token = os.getenv('GL_OAUTH_TOKEN', None)
    # gl = Gitlab(server, private_token=private_token, oauth_token=oauth_token)
    # return gl
    return GitLabAccess.from_environment()


class GitLabProject:
    def __init__(self, access, json_data):
        self.access: GitLabAccess = access
        self.full_path = json_data['full_path']
        self.label_name = json_data['label_name']
        # use this urlencoded project path as the project ID
        # until we learn what the numeric ID is.
        self.escaped_path = _gitlab_urlencode(self.full_path)
        self.__project_id = None
        self.__logger = make_logger(
            __name__, self, self.full_path.replace('/', '.'))

    @property
    def project_id(self):
        """
        Get the project ID.

        Either the numeric ID, if known, otherwise the urlencoded name.
        """
        if not self.__project_id:
            return self.escaped_path
        return self.__project_id

    @project_id.setter
    def project_id(self, val: Optional[int]):
        """Set the ID of this project."""
        if val and isinstance(val, int):
            self.__project_id = val

    @property
    def numeric_id(self):
        """Get the numeric ID of this project, if known, otherwise None."""
        return self.__project_id

    @numeric_id.setter
    def numeric_id(self, val: int):
        """Set the numeric ID of this project."""
        self.project_id = val
        return self.project_id

    async def get_gitlab_item(self, gl: gidgetlab.abc.GitLabAPI,
                              url: str, params: Dict = {}):
        """Call gl.getitem() after logging and prepending the project ID."""
        if not url.startswith('/projects'):
            url = f"/projects/{self.project_id}" + url

        if params:
            self.__logger.debug('GET on %s with params %s', url, str(params))
        else:
            self.__logger.debug('GET on %s', url)
        return await gl.getitem(url, params)

    def get_gitlab_item_sync(self, url: str, params: Dict = {}):
        """Synchronously call gl.getitem() after logging and prepending the project ID."""
        async def do_it(gl):
            return await self.get_gitlab_item(gl, url, params)
        return self.access.call_with_gitlab(do_it)

    async def post_gitlab(self, gl: gidgetlab.abc.GitLabAPI, url: str, data=None):
        """Call gl.post() after logging and prepending the project ID."""
        if not url.startswith('/projects'):
            url = f"/projects/{self.project_id}" + url

        self.__logger.debug(f'POST to {url} of {data}')
        return await gl.post(url, params={}, data=data)

    def post_gitlab_sync(self, url: str, params: Dict = {}):
        """Synchronously call gl.post() after logging and prepending the project ID."""
        async def do_it(gl):
            return await self.post_gitlab(gl, url, params)
        return self.access.call_with_gitlab(do_it)

    async def put_gitlab(self, gl: gidgetlab.abc.GitLabAPI, url: str, data=None):
        """Call gl.put() after logging and prepending the project ID."""
        if not url.startswith('/projects'):
            url = f"/projects/{self.project_id}" + url

        self.__logger.debug(f'PUT to {url} of {data}')
        return await gl.put(url, data=data)

    def put_gitlab_sync(self, url: str, params: Dict = {}):
        """Synchronously call gl.put() after logging and prepending the project ID."""
        async def do_it(gl):
            return await self.put_gitlab(gl, url, params)
        return self.access.call_with_gitlab(do_it)

    async def delete_gitlab(self, gl: gidgetlab.abc.GitLabAPI, url: str):
        """Call gl.delete() after logging and prepending the project ID."""
        if not url.startswith('/projects'):
            url = f"/projects/{self.project_id}" + url

        self.__logger.debug(f'DELETE of {url}')
        return await gl.delete(url)

    def delete_gitlab_sync(self, url: str):
        """Synchronously call gl.delete() after logging and prepending the project ID."""
        async def do_it(gl):
            return await self.delete_gitlab(gl, url)
        return self.access.call_with_gitlab(do_it)

    def make_issue_link(self, issue_num):
        return f'{self.access.url}/{self.full_path}/issues/{issue_num}'


def _openxr():
    return GitLabProject(make_gl_client(),
                         {'full_path': "openxr/openxr", 'label_name': 'From GitHub'})


if __name__ == "__main__":
    # gl_access = make_gl_client()
    config_logger()
    proj = _openxr()
    print(proj.get_gitlab_item_sync("/issues"))
