#!/usr/bin/env python3
# Copyright 2020-2021 Collabora, Ltd.
#
# SPDX-License-Identifier: Apache-2.0
"""Main entry point."""

import click
from dotenv import load_dotenv

from . import check_label as check_label_impl
from . import (data, github, gitlab, reverse_sync_closed, sync_finish,
               sync_start)
from .log import config_logger
from .sync_start import CONFIG as DEFAULT_CONFIG_FILENAME

load_dotenv()


@click.group()
@click.option("-c", "--config",
              "config_file",
              type=click.Path(file_okay=True, dir_okay=False, readable=True),
              default="config.json",
              show_default=True,
              help="Config filename")
@click.option("-d", "--data",
              "data_file",
              type=click.Path(file_okay=True, dir_okay=False,
                              readable=True, writable=True),
              default="data.json",
              show_default=True,
              help="The name of the JSON file used to store persistent state data.")
@click.option("-v", "--verbose",
              "verbose",
              count=True,
              help="Show verbose info messages. Repeat for more verbosity.")
@click.option("--gl-user",
              envvar="GL_USERNAME",
              show_envvar=True,
              nargs=1,
              help="The username to use when accessing GitLab.")
@click.option("--gl-token",
              envvar="GL_ACCESS_TOKEN",
              show_envvar=True,
              hide_input=True,
              nargs=1,
              help="The access token to use when accessing GitLab.")
@click.option("--gl-url",
              envvar="GL_URL",
              show_envvar=True,
              default="https://gitlab.com",
              show_default=True,
              help="The GitLab instance URL.")
@click.option("--gh-token",
              envvar="AUTH_TOKEN",
              show_envvar=True,
              hide_input=True,
              nargs=1,
              help="The access token to use when accessing GitHub.")
@click.pass_context
def cli(ctx, config_file, data_file, verbose,
        gl_user, gl_token, gl_url, gh_token):
    """Issue-Sync helps maintain related projects across GitLab and GitHub."""
    # fmt = "[%(levelname)s:%(name)s]  %(message)s"
    config_logger(verbosity=verbose)
    ctx.obj = {}
    ctx.obj["d"] = data.Data(data_file)
    ctx.obj["config"] = data.Config(config_file)
    ctx.obj["gh_client"] = github.make_gh_client(gh_token)
    ctx.obj["gl_access"] = gitlab.GitLabAccess(gl_url, gl_user, gl_token)


@cli.command()
@click.option("-s", "--sync-config",
              "sync_config_file",
              type=click.Path(file_okay=True, dir_okay=False, writable=True),
              default=DEFAULT_CONFIG_FILENAME,
              show_default=True,
              help="The name of the JSON file to write for human review. "
              "The same must be passed to the 'finish' command.")
@click.pass_context
def start(ctx, sync_config_file):
    """Find new issues on GitHub and write them to JSON."""
    sync_start.start(
        d=ctx.obj["d"],
        config=ctx.obj["config"],
        gh_client=ctx.obj["gh_client"],
        sync_file_name=sync_config_file
    )


@cli.command()
@click.option("-s", "--sync-config",
              "sync_config_file",
              type=click.Path(file_okay=True, dir_okay=False, readable=True),
              default=DEFAULT_CONFIG_FILENAME,
              show_default=True,
              help="The name of the JSON file to write for human review. "
              "The same must be passed to the 'finish' command.")
@click.option("-n", "--dryrun",
              "dry_run",
              is_flag=True,
              help="Skips actually making changes to the project.")
@click.pass_context
def finish(ctx, sync_config_file, dry_run):
    """Create GitLab issues for each new GitHub issue in the JSON."""
    if dry_run:
        print("Doing a dry-run")
    sync_finish.finish(
        d=ctx.obj["d"],
        config=ctx.obj["config"],
        gh_client=ctx.obj["gh_client"],
        gl_access=ctx.obj["gl_access"],
        sync_file_name=sync_config_file,
        dry_run=dry_run
    )


@cli.command()
@click.option("-s", "--sync-config",
              "sync_config_file",
              type=click.Path(file_okay=True, dir_okay=False, writable=True),
              default=DEFAULT_CONFIG_FILENAME,
              show_default=True,
              help="The name of the JSON file to write for human review. "
              "The same must be passed to the 'finish' command.")
@click.pass_context
def check_label(ctx, sync_config_file):
    """Find issues labeled as synced but not in our data file."""
    check_label_impl.check_label(
        d=ctx.obj["d"],
        config=ctx.obj["config"],
        gh_client=ctx.obj["gh_client"],
        sync_config_fn=sync_config_file
    )


@cli.command()
@click.pass_context
def sync_closed(ctx):
    """Close GitLab issues whose GitHub issues are closed."""
    reverse_sync_closed.reverse_sync(
        d=ctx.obj["d"],
        config=ctx.obj["config"],
        gh_client=ctx.obj["gh_client"],
        gl_access=ctx.obj["gl_access"]
    )
